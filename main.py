import copy
import glob
import logging
import sys

import h5py
import numpy as np

name2num = {
    "xpeak": 1,
    "zsigma": 2,
    "zemmitance": 3,
    "flux": 4,
    "xsigma": 5,
    "zpeak": 6,
    "xemmitance": 7,
}


class HDDataset:
    def __init__(self, log_level=logging.DEBUG):
        self.logger = logging.getLogger("tipper")
        self.logger.setLevel(log_level)
        self.logger.addHandler(logging.StreamHandler(sys.stdout))
        self.pinhole_dataset = None
        self.pinhole_dataset_meta = None
        self.pinhole_timestamps = None
        self.scalars_dataset = None
        self.scalars_dataset_meta = None
        self.dataset = None
        self.labels = None
        self.pinhole_dir = None
        self.scalars_dir = None

    def add_paths(self, pinhole_dir, scalars_dir):
        self.pinhole_dataset = None
        self.pinhole_dataset_meta = None
        self.pinhole_timestamps = None
        self.scalars_dataset = None
        self.scalars_dataset_meta = None
        self.dataset = None
        self.labels = None
        self.pinhole_dir = pinhole_dir
        self.scalars_dir = scalars_dir

    def stack_values(self):
        self.stack_images()
        self.stack_scalars()

    def stack_images(self):
        files = glob.glob(self.pinhole_dir + "/*.h5")
        self.logger.debug(f"Image files: {files}")
        f = h5py.File(files[0], "r")
        self.pinhole_dataset = f[("images")][()]
        self.pinhole_dataset_meta = f[("meta")][()]
        files.pop(0)
        for file in files:
            f = h5py.File(file, "r")
            self.pinhole_dataset = np.concatenate(
                (self.pinhole_dataset, f[("images")][()])
            )
            self.pinhole_dataset_meta = np.concatenate(
                (self.pinhole_dataset_meta, f[("meta")][()])
            )
            self.logger.debug(
                f"Stack shapes: imgs {self.pinhole_dataset.shape}, "
                f"imgs meta {self.pinhole_dataset_meta.shape}"
            )
        self.pinhole_timestamps = np.array(
            [el[0] for el in self.pinhole_dataset_meta]
        ).astype(np.float64)
        self.time_fixer()

    def stack_scalars(self):
        files = glob.glob(self.scalars_dir + "/*.h5")
        self.logger.debug(f"Scalar files: {files}")
        f = h5py.File(files[0], "r")
        self.scalars_dataset = f[("data")][()]
        self.scalars_dataset_meta = f[("meta")][()]
        files.pop(0)
        for file in files:
            f = h5py.File(file, "r")
            self.scalars_dataset = np.concatenate(
                (self.scalars_dataset, f[("data")][()])
            )
            self.logger.debug(f"Stack shapes: scalars {self.scalars_dataset.shape}")

    def correlate(self):
        """
        Method to be defined. Dummy implementation.
        :return:
        """
        scalar_timestamps = self.scalars_dataset[:, 0]
        first = True
        self.logger.debug(f"Correlating {self.pinhole_timestamps.shape} values")
        for id, timestamp in enumerate(self.pinhole_timestamps):
            ind = np.argmin(abs(scalar_timestamps - timestamp))
            if first:
                self.dataset = np.insert(self.scalars_dataset[ind, 1:], 0, timestamp)
                first = False
            else:
                row = np.insert(self.scalars_dataset[ind, 1:], 0, timestamp)
                self.dataset = np.vstack((self.dataset, row))
            # self.logger.debug(f"Correlated {id} value")

    def evaluate(self):
        tmp1 = np.logical_and(
            self.dataset[:, name2num["xemmitance"]] < 11,
            self.dataset[:, name2num["xemmitance"]] > 5,
        )
        tmp2 = np.logical_and(
            self.dataset[:, name2num["zemmitance"]] < 0.08,
            self.dataset[:, name2num["zemmitance"]] > 0.05,
        )
        self.labels = np.logical_not(np.logical_and(tmp1, tmp2))

    def save(self, name):

        hf = h5py.File(f"D:\Baza danych\Threshold_test\wider\{name}.h5", "w")
        hf.create_dataset("scalar_data", data=self.dataset)
        hf.create_dataset("image_data", data=self.pinhole_dataset, compression="gzip")
        hf.create_dataset("labels", data=self.labels)
        hf.close()
        print(f"Saved!!!")

    def plot_hist(self):
        import matplotlib.pyplot as plt

        plt.hist(self.labels.astype(int))
        plt.title("Classes")
        plt.show()

    def time_fixer(self):
        """
        Pinhole images may have epoch values corrupted (around 40 the same values in the row, needs to be fixed).
        Probably archiving error.
        """
        self.logger.debug(f"Fixing {len(self.pinhole_timestamps)} epochs...")
        epochs = list(set(self.pinhole_timestamps))
        for i, epoch in enumerate(epochs):
            occurances = self.pinhole_timestamps == epoch
            if sum(occurances) > 2:  # at least three the same
                indexes = [i for i, x in enumerate(occurances) if x]
                if (
                    not indexes[-1] == len(self.pinhole_timestamps) - 1
                ):  # if at the end we do not have reference for linspace
                    new_ones = np.linspace(
                        self.pinhole_timestamps[indexes[0]],
                        self.pinhole_timestamps[indexes[-1] + 1],
                        len(indexes),
                        endpoint=False,
                    )
                    self.pinhole_timestamps[occurances] = np.round(new_ones)
        self.logger.debug("Epochs fixed!")


def main():
    inst = HDDataset(log_level=logging.DEBUG)
    for i in range(25):
        print(f"Doing {i+1} value")
        inst.add_paths(
            f"D:\Baza danych\pinhole\{i + 1}", f"D:\Baza danych\scalars\{i + 1}"
        )
        inst.stack_values()
        inst.correlate()
        # choose from 'xpeak'  'zsigma' 'zemmitance' 'flux' 'xsigma' 'zpeak' 'xemmitance'
        inst.evaluate()
        inst.plot_hist()
        inst.save(f"dataset_fixed_part{i+1}")

    print("Done")


if __name__ == "__main__":
    main()
