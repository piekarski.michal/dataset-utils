import h5py
import tkinter as tk
from tkinter import simpledialog
from PIL import Image
import numpy as np
import datetime
from matplotlib import cm

num2name = {
    1: "xpeak",
    2: "zsigma",
    3: "zemmitance",
    4: "flux",
    5: "xsigma",
    6: "zpeak",
    7: "xemmitance",
}

ROOT = tk.Tk()
ROOT.withdraw()

hf = h5py.File("D:\Baza danych\Threshold_test\dataset_part1.h5", "r")


def prepare():
    n1 = hf.get("scalar_data")
    n2 = hf.get("image_data")
    n3 = hf.get("labels")
    return n1, n2, n3


def main():
    data = prepare()
    scalars = np.array(data[0])
    xem = np.array([x[7] for x in scalars])
    zem = np.array([x[3] for x in scalars])
    xwhere = xem < 10
    xwhere2 = xem > 5
    fin = np.logical_and(xwhere, xwhere2)
    print(np.argwhere(fin == True))
    while True:
        try:
            USER_INP = simpledialog.askstring(
                title="Test dataset", prompt="Które pokazać?"
            )
            scalar = data[0][int(USER_INP)]
            img = data[1][int(USER_INP)]
            label = data[2][int(USER_INP)]
            imag = Image.fromarray(np.uint8(cm.gist_earth(img) * 255))
            # imag.save(f"pin_OK_{USER_INP}_xem_9_97512_zem_0_07055.png")
            imag.save(f"inputimage.png")
            imag.show()
            for i, s in enumerate(scalar):
                if i == 0:
                    print(
                        f"Timestamp: {datetime.datetime.fromtimestamp(s).strftime('%c')}"
                    )
                else:
                    print(f"{num2name[i]}, {s}")
            print(f"Flag: {label}")
        except Exception as e:
            print("Bye!")
            break


if __name__ == "__main__":
    main()
